package com.example.user.gametest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by USER on 22-06-2017.
 */

//Use this class as MainView on which other Views will be drawn.
public class GameView extends SurfaceView implements Runnable {


    volatile boolean playing;
    private int maxX;
    private int maxY;
    private Paint painter;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;

    private Thread MainGame = null;

    private ArrayList<Clouds> clouds;
    private int cloudCount;
    private Player player;


    private ArrayList<Obstructions> obstructions;
    private int obsCount;


    public GameView(Context context, int maxX, int maxY) {
        super(context);
        this.maxX = maxX;
        this.maxY = maxY;


        clouds = new ArrayList<>();
        obstructions = new ArrayList<>();


        painter = new Paint();
        surfaceHolder = getHolder();

        int min = 6;
        Random r = new Random();
        cloudCount = r.nextInt(3) + min;
        obsCount = r.nextInt(3) + 3;


        for (int i = 0; i < cloudCount; i++) {
            Clouds cld = new Clouds(context, maxX, maxY);
            clouds.add(cld);
        }
        player = new Player(context, maxX, maxY);


        for (int i = 0; i < obsCount; i++) {
            Obstructions ob = new Obstructions(context, maxX, maxY);
            obstructions.add(ob);
        }


    }

    @Override
    public void run() {
        while (playing) {
            update(); // updates the changes in coordinates
            draw();    // draws the updated changes
            gameRun();  // sets the fps for our game ie how many frames per second
        }
    }


    private void update() {   // will do mostly player and enemy work ie position change or collision detection

        player.update();

        for (Obstructions ob : obstructions) {
            ob.obsMove(3);
        }

        for (Clouds c : clouds) {
            c.moveCloud(3);
        }


        for (Obstructions ob : obstructions) {

            if (Rect.intersects(player.getRect(), ob.getRect())) {
             Log.i("Collision", "collided");
                break;
            }

        }

    }

    private void draw() {       // will draw the updated view
        if (surfaceHolder.getSurface().isValid()) {

            canvas = surfaceHolder.lockCanvas();
            canvas.drawColor(Color.argb(255, 170, 230, 249));


            for (Clouds c : clouds) {
                canvas.drawBitmap(c.getBitmap(), c.getScreenX(), c.getScreenY(), painter);
            }

            for (Obstructions ob : obstructions) {
                canvas.drawBitmap(ob.getbitmap(), ob.getScreenX(), ob.getScreenY(), painter);
            }

            //Drawing the player
            canvas.drawBitmap(
                    player.getBitmap(),
                    player.getX(),
                    player.getY(),
                    painter);

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void gameRun() {    // for controling game runtime.
        try {
            MainGame.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pause() {     // pause button or Activity pause Action (eg. HomeButton mobile, Incoming call ,etc.)
        playing = false;
        try {
            MainGame.join();   // There will be multiple threads. What join() will do is that it will pause the concerned thread and
        } catch (InterruptedException e) {// it will also not allow other threads to run until this thread resumes and finishes. Therefore
        }                                  // this ensures that full game and all threads are paused.
    }

    public void resume() {  // called when resume button is clicked. Will reStart the MainGame Thread.
        playing = true;
        MainGame = new Thread(this);
        MainGame.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        player.setStarting();
        switch (event.getAction() & event.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                player.accelerating();
                break;
            case MotionEvent.ACTION_DOWN:
                player.deaccelarating();
                break;
        }

        return true;
    }
}