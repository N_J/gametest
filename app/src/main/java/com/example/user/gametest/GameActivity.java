package com.example.user.gametest;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;

/**
 * Created by USER on 22-06-2017.
 */
public class GameActivity extends AppCompatActivity {

    private GameView gameView;
    private Clouds clouds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display screen = getWindowManager().getDefaultDisplay();

        Point screenDimen= new Point();
        screen.getSize(screenDimen);
        int maxX=screenDimen.x;
        int maxY=screenDimen.y;

        clouds = new Clouds(this,maxX,maxY);


        gameView = new GameView(this, maxX,maxY);
        setContentView(gameView);   //setting the content where whatever is drawn on GameView will be displayed for this Activity
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }


}

