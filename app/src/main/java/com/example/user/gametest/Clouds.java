package com.example.user.gametest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.Random;

/**
 * Created by USER on 22-06-2017.
 */

public class Clouds {

    private int screenMaxX, screenMaxY;
        private int screenX, screenY;
    private Bitmap bitmap;

    public Clouds(Context context, int screenMaxX, int screenMaxY) {

        this.screenMaxX = screenMaxX;
        this.screenMaxY = screenMaxY;

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.cloud);


        Random starsPos = new Random();

        screenX = starsPos.nextInt(screenMaxX) - (bitmap.getWidth()+10);
        screenY = starsPos.nextInt(screenMaxY) - bitmap.getHeight();


    }


    public void moveCloud(int speed) {

        Random starsPos = new Random();

        screenX -= speed;

        if (screenX <= (0 - bitmap.getWidth())) {

            screenX = screenMaxX;
            screenY = starsPos.nextInt(screenMaxY) - bitmap.getHeight();


        }


    }

    public int getScreenX() {
        return screenX;
    }


    public int getScreenY() {
        return screenY;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
