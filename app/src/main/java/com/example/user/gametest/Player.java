package com.example.user.gametest;

/**
 * Created by Rakhi on 25-06-2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created by Belal on 6/24/2016.
 */
public class Player {
    //Bitmap to get character from image
    private Bitmap bitmap;

    //coordinates
    private int x;
    private int y;

    //max and min speed of player
    private int MAX_SPEED = 10;
    private int MIN_SPEED = 1;

    //speed of player
    private int speed;

    private final int GRAVITY = -5;
    private int minY, maxY;

    private Canvas canvas;
    //a boolean value speeding to increase or decrease speed by calling functions accelerating and deaccelearting the speed
    private Boolean speeding;

    private Rect PlayerRect;
    private boolean starting;

    //constructor
    public Player(Context context, int screenX, int screenY) {
        x = screenX / 2 - 100;
        y = screenY / 2 - 100;

        speed = 1;
        minY = 0;

        //Getting bitmap from drawable resource
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.bird);


        maxY = screenY - bitmap.getHeight() - 20;//to make the bird visible at the bottom when it reaches max Y


        // initially speeding is set to false that is player neither acc or deacc
        speeding = false;
        starting = false;

        PlayerRect = new Rect(x, y, x + bitmap.getWidth(), y + bitmap.getHeight());

    }

    public void accelerating() {
        speeding = true;
    }

    public void deaccelarating() {
        speeding = false;
    }
    //update method used to- 1. if player touched increase speed and on releasing decrease speed. 2.then if speed increases max speed
    //bring it to max speed and same with max speed. 3.and also updating y so that player does not go out of screen

    public void update() {

        if (starting) {
            if (speeding) {
                speed = speed + 5;//this speed increases bird speed in y direction

            } else {
                speed = speed - 2;
            }
            //controlling the top speed
            if (speed > MAX_SPEED) {
                speed = MAX_SPEED;
            }
            //if the speed is less than min speed
            //controlling it so that it won't stop completely
            if (speed < MIN_SPEED) {
                speed = MIN_SPEED;
            }

            //moving the ship down

            y = y + GRAVITY + speed;

            //but controlling it also so that it won't go off the screen
            if (y < minY) {
                y = minY;
            }
            if (y > maxY) {
                y = maxY;
            }


            PlayerRect.left = x;
            PlayerRect.right = x + bitmap.getWidth();
            PlayerRect.top = y;
            PlayerRect.bottom = y + bitmap.getHeight();

        }
    }

    /*
    * These are getters you can generate it autmaticallyl
    * right click on editor -> generate -> getters
    * */
    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpeed() {
        return speed;
    }

    public Rect getRect() {
        return PlayerRect;
    }

    public void setStarting(){
        this.starting=true;
    }

}