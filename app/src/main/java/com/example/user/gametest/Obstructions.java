package com.example.user.gametest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by USER on 25-06-2017.
 */
public class Obstructions {

    private int screenMaxX, screenMaxY;
    private int screenX, screenY;
    private Bitmap bitmap,bitmap1;
    private boolean upOrDown;
    private int speed;

    private Rect ObsRect;

    public Obstructions(Context context, int screenMaxX, int screenMaxY) {

        this.screenMaxX = screenMaxX;
        this.screenMaxY = screenMaxY;

        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.obstruction);
         Random obsPos = new Random();

        screenX = obsPos.nextInt(screenMaxX) - bitmap.getWidth();
        screenY = obsPos.nextInt(screenMaxY) - bitmap.getHeight();

        //    speed = obsPos.nextInt(3);

//        if (speed > 0)
//            upOrDown = true;
//        else
//            upOrDown = false;


        ObsRect = new Rect(screenX, screenY, screenX + bitmap.getWidth(), screenY + bitmap.getHeight());

    }


    public void obsMove(int s) {
        Random obsPos = new Random();

//
//        if (upOrDown) {
//            screenY = screenY - speed;
//
//            if (screenY <= (0 - bitmap.getHeight())) {
//                screenY = screenMaxY;
//            }
//
//        } else {
//            screenY = screenY + speed;
//            if ((screenY >= screenMaxY)) {
//                screenY = 0 - bitmap.getHeight();
//            }
//
//        }
//

        screenX = screenX - s;

        if (screenX <= (0 - bitmap.getWidth())) {

            screenX = screenMaxX;
            //          speed = obsPos.nextInt(3);
            screenY = obsPos.nextInt(screenMaxY) - bitmap.getHeight();

//            upOrDown = !upOrDown;


        }


        ObsRect.left = screenX;
        ObsRect.right = screenX + bitmap.getWidth();
        ObsRect.top = screenY;
        ObsRect.bottom = screenY + bitmap.getHeight();

    }


    public int getScreenX() {
        return screenX;
    }


    public int getScreenY() {
        return screenY;
    }


    public Bitmap getbitmap() {
        return bitmap;
    }




    public Rect getRect() {
        return ObsRect;
    }


}
